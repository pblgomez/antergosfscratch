#!/usr/bin/env bash
set -e

yay -Rsn --noconfirm virtualbox-guest-modules-dkms
yay -Syyuu --needed --noconfirm virtualbox-guest-utils virtualbox-guest-modules-arch linux-headers
echo "----------------------------------------------------------------------"
echo "     Done, you probably neede to reboot"
echo "----------------------------------------------------------------------"
