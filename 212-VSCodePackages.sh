#!/usr/bin/env bash
set -e

if [ -f /usr/bin/code ]; then 
	while IFS= read -r package
	do
		if ls $HOME/.vscode-oss/extensions/$package* 1> /dev/null 2>&1; then
			echo "Already installed $package"
		else
			echo "Installing $package"
			code --install-extension $package
		fi
	done < vscode-packages.txt
fi

echo "----------------------------------------------------------------------"
echo "     Configuring VSCode..."
echo "----------------------------------------------------------------------"
if hash code 2>/dev/null; then
  if grep -q '"editor.lineNumbers": "relative","' "$HOME/.config/Code - OSS/User/settings.json"; then
      echo "Nothing to do"
      else
      sed -i '/}/i \    "editor.lineNumbers": "relative",' "$HOME/.config/Code - OSS/User/settings.json"
  fi
fi 
if [ -f $HOME/.config/VSCodium/User/settings.json ]; then
  if grep -q '"editor.lineNumbers": "relative","' "$HOME/.config/VSCodium/User/settings.json"; then
      echo "Nothing to do"
      else
      sed -i '/}/i \    "editor.lineNumbers": "relative",' "$HOME/.config/VSCodium/User/settings.json"
  fi
fi

