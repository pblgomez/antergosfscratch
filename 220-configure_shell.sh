#!/usr/bin/env bash
set -e

echo "----------------------------------------------------------------------"
echo "     Selecting shell"
echo "----------------------------------------------------------------------"
if [ -f /usr/bin/fish ] && [ $SHELL != /usr/bin/fish ]; then
  chsh -s `which fish`
fi
