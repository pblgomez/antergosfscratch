#!/usr/bin/env bash
set -e

echo "----------------------------------------------------------------------"
echo "     Installing i3"
echo "----------------------------------------------------------------------"
yay -S --needed --noconfirm i3-gaps rxvt-unicode dmenu rxvt-unicode-terminfo compton polybar betterlockscreen blurlock
yay -S --needed --noconfirm i3lock-color ## disabled packages i3lock i3status


if [ -f /usr/bin/betterlockscreen ]; then
  if [ -f $HOME/.config/i3/lockscreen.jpg ]; then
    betterlockscreen -u $HOME/.config/i3/lockscreen.jpg
  elif [ -f /usr/share/lightdm-webkit/themes/antergos/img/fallback_bg.jpg ]; then
    betterlockscreen -u /usr/share/lightdm-webkit/themes/antergos/img/fallback_bg.jpg
  elif [ -f /usr/share/antergos/wallpapers/white_line_by_snipes2.jpg ]; then
    betterlockscreen -u /usr/share/antergos/wallpapers/white_line_by_snipes2.jpg
  fi
fi
