#!/usr/bin/env bash
  
DEVICE=$(xinput list | grep Touchpad | cut -f2 | cut -d "=" -f2)


DEVICEPROP=$(xinput list-props $DEVICE | grep "Tapping Enabled (" | cut -d "(" -f2 | cut -d ")" -f1)

xinput set-prop $DEVICE $DEVICEPROP 1