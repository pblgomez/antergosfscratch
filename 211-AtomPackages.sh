#!/usr/bin/env bash
set -e

while IFS= read -r package
do
	if [ ! -d $HOME/.atom/packages/$package ]; then
		echo "Installing $package";
		apm install $package
	fi
done < atom-packages.txt


if groups $USER | grep &>/dev/null '\block\b'; then
	echo ""
else
    echo "Adding user $USER to group lock for arduino programming"
    sudo gpasswd -a $USER lock
fi

if groups $USER | grep &>/dev/null '\buucp\b'; then
	echo ""
else
    echo "Adding user $USER to group uucp for arduino programming"
    sudo gpasswd -a $USER uucp
fi
