#------------------------------------------------------------
#     System
#------------------------------------------------------------
acpi
atom
awesome-terminal-fonts
bc
clang
compton-tryone-git
feh
fish
gnome-alsamixer
imagemagick
mpv
pavucontrol
pulseaudio
python-pywal
snapd
stow
systemd-resolvconf
tlp
vim
vim-plug
xorg-xbacklight
xorg-xinput
xorg-xrdb
#network-manager-applet-indicator

#------------------------------------------------------------
#     Apps
#------------------------------------------------------------
atom
code
geoclue
firefox
feh
flameshot
keepassxc
openssh
dunst
redshift
unclutter
variety
wireguard-arch
wireguard-tools

#------------------------------------------------------------
#     Fonts
#------------------------------------------------------------
otf-fira-code
ttf-inconsolata
ttf-iosevka-term
ttf-ubuntu-font-family
ttf-bitstream-vera

#------------------------------------------------------------
#     Languages
#------------------------------------------------------------
hunspell-es_es
