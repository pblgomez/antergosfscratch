#!/usr/bin/env bash
set -e

echo "----------------------------------------------------------------------"
echo "     Enabling services"
echo "----------------------------------------------------------------------"
if [ -f /usr/bin/snap ];then
  echo "----------------------------------------------------------------------"
  echo "     Enabling snapd"
  echo "----------------------------------------------------------------------"
  sudo systemctl start snapd.socket
  sudo systemctl start snapd.service
  sudo systemctl enable snapd.socket
  sudo systemctl enable snapd.service
fi
