#!/usr/bin/env bash
set -e

# echo "----------------------------------------------------------------------"
# echo "     Configuring dotfiles (stow)"
# echo "----------------------------------------------------------------------"
# 
# ## Backup old i3 config
# if [ ! -L $HOME/.config/i3/config ]; then mv $HOME/.config/i3/config $HOME/.config/i3/config.bak; fi
# 
# 
# ## Create dir for Wallpapers
# mkdir -p $HOME/Images/Wallpapers
# 
# stow --restow `ls -d */`

# Update for the first time betterlockscreen
if [ -f /usr/bin/betterlockscreen ]; then
  betterlockscreen -u $HOME/.config/i3/lockscreen.jpg
fi

cd ~
yadm clone https://gitlab.com/pblgomez/dotfiles
